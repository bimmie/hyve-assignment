#!/usr/bin/env python3

from disutils.core import setup

setup(name='hyve_assignment',
      version='1.0',
      description='Hyve Programming Assignment #2',
      author='James Collier',
      author_email='james.collier412@gmail.com',
      url='https://gitlab.com/bimmie/hyve-assignment',
      packages=['encoder', 'decoder']
     )
