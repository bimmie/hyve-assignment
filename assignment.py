#!/usr/bin/env python3

#
# Expects data from stdin in the format e.g. \x00\x20\x00\x2A\x02\x01
# ASSUMPTION: Prints a newline character after printing the encoded/decoded
#             output
#

import os
import sys
import ast
from encoder import encoder
from decoder import decoder

if __name__ == "__main__":
    #Here's our polymorphic bit
    enc = None
    if "USE_TRIVIAL_IMPLEMENTATION" in os.environ:
        enc = encoder.simple_encode
    else:
        enc = encoder.encode


    #read the encoded data stream
    rawdata = ast.literal_eval("'"+sys.stdin.read().strip()+"'").encode('utf-8')
    decoded_data = decoder.decode(rawdata)
    sys.stdout.write('\\x'+'\\x'.join(format(x, '02x') \
                                      for x in decoded_data)+'\n')
    sys.stderr.write('\\x'+'\\x'.join(format(x, '02x') \
                                      for x in enc(decoded_data))+'\n')
