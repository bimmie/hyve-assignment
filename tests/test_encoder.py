import os
import sys
sys.path.insert(0, '')

from encoder import encoder


def test_encode_complex():
    assert encoder.encode(b'\x20\x2A\x20') == b'\x00\x20\x00\x2A\x02\x01'

def test_encode_simple():
    assert encoder.simple_encode(b'\x20\x2A\x20') == b'\x00\x20\x00\x2A\x00\x20'


if __name__ == '__main__':
    test_encode_complex()
    test_encode_simple()
