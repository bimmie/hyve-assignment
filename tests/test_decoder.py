import os
import sys
sys.path.insert(0, '')

from decoder import decoder


def test_decode():
    assert decoder.decode(b'\x00\x20\x00\x2A\x02\x01') == b'\x20\x2A\x20'
    assert decoder.decode(b'\x00\x20\x00\x2A\x00\x20') == b'\x20\x2A\x20'

def test_decode_incomplete():
    assert decoder.decode(b'\x00') == b'\x3f'
    assert decoder.decode(b'\x00\x20\x00\x2A\x02\x01\x53') == b'\x20\x2A\x20\x3F'

def test_decode_empty():
    assert decoder.decode(b'') == b''

if __name__ == '__main__':
    test_decode()
    test_decode_incomplete()
    test_decode_empty()
