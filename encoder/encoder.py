"""
This file implements the data encoder interface
"""

def _encode(data, index):
    """
    Internal implementation of the complex encoder
    Recursively generates the encoded output from the input data stream
    Input: data: a stream of bytes to encode
    Input: index: the index into the stream of bytes to begin encoding from
    """
    skip = 1
    q = 0
    p = 0

    if index == len(data):
        return b''
    elif len(data) < 2:
        return b'\x3F'
    else:
        q = data[index]
        prefix = data[:index]
        for i in range(len(data), index, -1):
            suffix = data[index:i]
            exists = prefix.find(suffix)
            if exists != -1:
                skip = len(suffix)
                p = index - exists 
                q = len(suffix)
                break

    #encode q,p
    return bytes([p,q]) + _encode(data, index+skip)

def encode(data):
    """
    Encode a stream of bytes using the complex method
    """
    return _encode(data, 0)

def simple_encode(data):
    """
    Encode a stream of bytes using the simple method
    """
    stream = b''
    for byte in data:
        stream += b'\x00' + bytes([byte])

    return stream
