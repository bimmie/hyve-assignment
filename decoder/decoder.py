"""
This file implements the data decode interface
"""

def decode(data):
    """
    Decode an encoded datastream
    A valid datastream has a length which is a multiple of 2 bytes
    If this requirement is not the case, the decoder will append b'\x3f'
    to the output
    """
    stream = b''
    for i in range(0, len(data), 2):
        try:
            p, q = data[i:i+2]
            if p == 0:
                stream += bytes([q])
            else:
                stream += stream[len(stream)-p:len(stream)-p+q]
        except ValueError:
            stream += b'\x3F'

    return stream
