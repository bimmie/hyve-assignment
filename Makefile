init:
	pip3 install -r requirements.txt

test:
	pytest-3
	tests/example-run-1
	tests/example-run-2

.PHONY: init test
