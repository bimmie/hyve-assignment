## Information for users
Clone this repository to retrieve the python code. On initial download, run ``make init`` to ensure all dependencies are met

Unit and functional tests can be run using ``make test``

Run the program using:
``python3 assignment.py < mydatafile.txt``